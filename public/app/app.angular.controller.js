(function(){
	var app = angular.module('todos', ['todos-management', "ngRoute"]);
	var loggedIn = sessionStorage.loggedIn;
	var todos = [];
	console.log(loggedIn);
	app.config(function($routeProvider) {
	    $routeProvider
	    .when("/", {
	        templateUrl : "views/register_login.html"
	    })
	    .when("/allTodos", {
	        templateUrl : "views/allTodos.html"
	    })
	    .when("/addNewTodo", {
	        templateUrl : "views/addNewTodo.html"
	    });
	});

	app.factory('AuthService', function($window) {
		var session = $window.sessionStorage;
		return {
			login : function() {
				session.setItem("isLogged",true);
			},
			logout : function() {
				sessionStorage.removeItem("isLogged");
			},
			isLoggedIn : function() {
				if (sessionStorage.getItem("isLogged")) {
					return true;
				}
				return false;
			}
		};
	});
	
	app.controller('RegistrationController', ['$http', function($http){
		this.userToRegister = {};
		this.registerUser = function(){
			if(!this.userToRegister.username || !this.userToRegister.email || !this.userToRegister.password){
				return;
			}
			$.ajax({
				url: '/Todos/register',
				type: 'post',
				data: this.userToRegister,
				dataType: 'json',
				crossDomain: true,
				complete: function(data){
					if(data.responseJSON.msg == 'success'){
						this.userToRegister = {};
						alert('Successfully registered');
					}
				}
			});
		};
	}]);
	
	app.controller('LoginController', ['$http', '$rootScope', '$location','AuthService', function($http, $rootScope, $location, AuthService){
		this.userToLogin = {};
		this.login = function(){
			$http.post('/Todos/login', this.userToLogin).then(function successCallback(response) {
				if(response.data == 'success'){
					AuthService.login();
					$http.get('/Todos/allTodos').then(function successCallback(resp) {
						console.log('successfully get all todos');
						todos.push.apply(todos, resp.data);
						$rootScope.allTodos = todos;
						$rootScope.user = true;
						$location.path( "/allTodos" );
					}, function errorCallback(resp) {
						console.error('an error occured while getting all todos'); 
					});
				}else{
					alert('Not correct credentials');
				}
			}, function errorCallback(response) {
				console.log(response); 
			});
		};
	}]);
	
	app.controller('AllTodosController', ['$rootScope','$http', function($rootScope, $http){
		$rootScope.allTodos = todos;
		this.refresh = function(){
			todos = [];
			$http.get('/Todos/allTodos').then(function successCallback(resp) {
				console.log('successfully get all todos');
				todos.push.apply(todos, resp.data);
				$rootScope.allTodos = todos;
				console.log(todos);
			}, function errorCallback(resp) {
				console.error('an error occured while getting all todos'); 
			});
		}
	}]);
	
	app.controller('LogoutController', ['$location', 'AuthService', '$rootScope', function($location, AuthService, $rootScope){
		this.userToLogin = {};
		this.logout = function(){
			AuthService.logout();
			$rootScope.user = false;
			todos = [];
			$location.path( "/" );
		}
	}]);
	

})();