(function(){
	var app = angular.module('todos-management', []);
	var todos = [];
	var todosId = 0;
	app.controller('ManageTodosController', ['$http', function($http){
		this.allTodos = todos;
		this.hide = false;
		this.todo = {
				title: '',
				done: false,
				id: todosId
		};
		this.addNewTodo = function(){
			if(!this.todo.title){
				return;
			}
			this.todo.id = todosId;
			todosId++;
			todos.push(this.todo);
			$http.put('/Todos/addTask', this.todo).then(function successCallback(resp) {
				console.log('successfully added new todo');
			}, function errorCallback(resp) {
				console.error('an error occured while adding new todo'); 
			});
			this.todo = {};
		};
		this.mark = function(id){
			var task = todos.find(function(element){
				return element.id == id;
			});
			if(!task){
				console.error('No task with such id. Id = ' + id);
				return;
			}
			console.log('To '+todos);
			task.done = !task.done;
			console.log('After '+todos);
			$http.put('/Todos/updateTask', task).then(function successCallback(resp) {
				console.log('successfully updated task');
			}, function errorCallback(resp) {
				console.error('an error occured while updating task'); 
			});
		};
		this.clear = function(){
			todos.splice(0, todos.length);
			todosId = 0;
		};
		this.remove = function(id){
			$http.put('/Todos/removeTask', {id: id}).then(function successCallback(resp) {
				$('#' + id).remove();
				console.log('successfully removed task');
			}, function errorCallback(resp) {
				console.error('an error occured while removing new task'); 
			});
		};
		this.removeAllDone = function(){
			var toRemove = todos.filter(function(item){
				return item.done;
			});
			todos = todos.filter(function(item){
				return !item.done;
			});
			toRemove.forEach(function(item){
				$('#' + item.id).remove();
			});
			$http.put('/Todos/removeAllDoneTasks', toRemove).then(function successCallback(resp) {
				console.log('successfully remove tasks');
			}, function errorCallback(resp) {
				console.error('an error occured while removing tasks'); 
			});
		};
	}]);
	app.controller('ShowHideController', function(){
		this.hide = false;
		this.showHideAll = function(){
			this.hide =! this.hide;
		};
	});
})();