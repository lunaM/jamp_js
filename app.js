var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var serviceName = 'Todos';
var cookieParser = require('cookie-parser');
var session = require('express-session');
var uuid = require('node-uuid');
var log4js = require('log4js');

app.use(cookieParser());
app.use(session({ 
	secret: 'lucia',
	saveUninitialized: true,
	resave: true,
	httpOnly: false
	}));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/Todos', function (req, resp) {
	resp.sendfile('public/views/index.html');
});

log4js.configure({
	appenders: [{type: 'file', filename:'logs/todos.log'}]
});
log4js.loadAppender('file');
var logger = log4js.getLogger();
logger.setLevel('ERROR');

var server = app.listen(8081, function () {
  var host = server.address().address;
  var port = server.address().port;
});
var mysql = require('mysql');
var connection = mysql.createPool({
  connectionLimit : 10,
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'todos'
});


// routes -----------------------------------------------------------------------------------
	
// work with user
	app.post('/' + serviceName + '/register', function(req, resp){
		var newUser = req.body;
		findSameUser(newUser.email, newUser.username, function(result){
			 	if(!result){
			 		registerUser(newUser, function(result){
			 			resp.send({msg:result});
					});
			 	}else{
			 		resp.send({msg:'error'});
			 	}
		});
	});
	app.post('/' + serviceName + '/login', function(req, resp){
		var loginUser = req.body;
		connection.query('SELECT * FROM user WHERE login LIKE ? AND password LIKE ?;', 
				[loginUser.username, loginUser.password], function(err, rows, fields) {
			  if (err || !rows[0]){
				  resp.send('error');
			  }else{
				  req.session.userId = rows[0].id;
				  resp.send('success');
			  }
			});
	});	
	//app.get('/' + serviceName + '/logout', function(req, resp){});
	
	//work with todos
	app.put('/' + serviceName + '/addTask', function(req, resp){
		var todo = req.body;
		if(todo.id == 0){
			req.session.currentListId = uuid.v1();
			connection.query('INSERT INTO todolist (id, user_id) VALUES (?, ?);', 
					[req.session.currentListId, req.session.userId], function(err, rows, fields){
				if(err){
					logger.error(err + '\n While inserting new todolist');
					resp.send('error');
				}
			});
		}
		connection.query('INSERT INTO task (status, task, id, todolist_id) VALUES (?, ?, ?, ?);',
				[false, todo.title, todo.id, req.session.currentListId], function(err, rows, fields){
				if(err){
					logger.error(err + '\nWhile inserting new todo');
					resp.send('error');
				}else{
					resp.send('success');
				}
		});
	});
	app.put('/' + serviceName + '/updateTask', function(req, resp){
		var task = req.body;
		connection.query('UPDATE task SET status = ? WHERE id = ? AND todolist_id LIKE ?;', 
				[task.done, task.id, req.session.currentListId], function(err, rows, fields){
			if(err){
				logger.error(err + '\nWhile updating task');
				resp.send('error');
			}else{
				resp.send('success');
			}
		});
	});
	app.put('/' + serviceName + '/removeTask', function(req, resp){
		connection.query('DELETE FROM task WHERE id = ? AND todolist_id LIKE ?;', 
				[req.body.id, req.session.currentListId], function(err, rows, fields){
			if(err){
				logger.error(err + '\nWhile removing task');
				resp.send('error');
			}else{
				resp.send('success');
			}
		});
	});
	app.put('/' + serviceName + '/removeAllDoneTasks', function(req, resp){
		var toRemoveTasks = req.body;
		var ids = [];
		var statement = 'DELETE FROM task WHERE id = ?';
		ids.push(toRemoveTasks[0].id);
		for(var i = 1; i < toRemoveTasks.length; i++){
			statement += ' OR id = ?';
			ids.push(toRemoveTasks[i].id);
		}
		connection.query(statement, ids, function(err, rows, fields){
			if(err){
				logger.error(err + '\nWhile removing all tasks');
				resp.send('error');
			}else{
				resp.send('success');
			}
		});
	});
	app.get('/' + serviceName + '/allTodos', function(req, resp){
		getAllTodoLists(req.session.userId, function(result){
			var allTodoLists = [];
			var currentTodo = {
				todoId: result[0].todolist_id,
				tasks: []
			};
			logger.info(result);
			for(var i = 0; i < result.length; i++){
				if(result[i].todolist_id != currentTodo.todoId){
					allTodoLists.push(currentTodo);
					currentTodo = {
						todoId: result[i].todolist_id,
						tasks: []
					}
				}
				currentTodo.tasks.push(result[i].task);
			}
			allTodoLists.push(currentTodo);
			resp.send(allTodoLists);
		});
	});

	
//functions to work with DB -----------------------------------------------------------------------------------------
	function getAllTodoLists(userId, callback){
		connection.query('SELECT * FROM task a JOIN todolist b ON a.todolist_id = b.id WHERE b.user_id = ?;',
				[userId], function(err, rows, fields) {
		  if (err || !rows[0]){
			  logger.error('An error occured while getting all todos');
			  callback('error');
		  }else{
			  callback(rows);
		  }
		});
	}
	
	function getAllTasks(todolistId, callback){
		connection.query('SELECT * FROM task WHERE todolist_id = ?;',
				[todolistId], function(err, rows, fields) {
		  if (err || !rows[0]){
			  logger.error('An error occured while getting all tasks');
			  callback('error');
		  }else{
			  callback(rows);
		  }
		});
	}
	
	//save new user
	function registerUser(user, callback){
		connection.query('INSERT INTO user (email,login,password) VALUES (?, ?, ?);',
				[user.email, user.username, user.password],	function(err, rows, fields) {
		  if (err){
			  logger.error('An error occured while adding new user');
			  callback('error');
		  }else{
			  callback('success');
		  }
		});
	}
	
	//find user by email or login
	function findSameUser(email, login, callback){
		connection.query('SELECT * FROM user WHERE email LIKE "' + email + '";', function(err, rows, fields) {
		  if (err){
			  callback(err);
			  logger.error('An error occured while searching user' + err);
		  }
		  if(rows[0]){
			  logger.info('Found user ' + rows[0]);
			  callback(rows[0]);
		  }else{
			  callback(false);
		  }
		});
	}
	
